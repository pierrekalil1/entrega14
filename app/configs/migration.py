from flask import Flask
from flask_migrate import Migrate

def init_app(app: Flask):
    from app.models.vacinado_model import Vaninado
   
    Migrate(app, app.db)