from app.configs.database import db
from dataclasses import dataclass
import datetime
from datetime import timedelta


@dataclass
class Vaninado(db.Model):
    keys = ['cpf', 'name', 'vaccine_name', 'health_unit_name']
    data_atual = datetime.datetime.utcnow()
    futuredate = datetime.datetime.utcnow() + timedelta(days=90)

    cpf: str
    name: str
    first_shot_date: str 
    second_shot_date: str
    vaccine_name: str
    health_unit_name:str

    __tablename__ = 'vaccine_cards'

    cpf = db.Column(db.String(11), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    first_shot_date = db.Column(db.DateTime(), default=data_atual.strftime(f'%d/%m/%Y %H:%M:%S'))
    second_shot_date = db.Column(db.DateTime(), default=futuredate)
    vaccine_name = db.Column(db.String(255), nullable=False)
    health_unit_name = db.Column(db.String(255))