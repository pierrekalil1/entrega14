from flask import Blueprint
from app.controllers.vaccination_controller import create_person_vaccinate, get_all

bp = Blueprint('vaccinations_bp', __name__, url_prefix='vaccinations')

bp.post('')(create_person_vaccinate)
bp.get('')(get_all)