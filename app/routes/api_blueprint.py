from . import vaccinator_blueprint
from flask import Blueprint

bp = Blueprint('api_bp', __name__, url_prefix='/api')

bp.register_blueprint(vaccinator_blueprint.bp)