from flask import jsonify, request, current_app
from app.models.vacinado_model import Vaninado
from http import HTTPStatus
from ipdb import set_trace


def create_person_vaccinate():
    data = request.get_json()
    
   
    try:
        list_keys = list(data.keys())
        set_trace()
        for key in list_keys:
            print(data[key])
            if key not in Vaninado.keys:
                del data[key]
        set_trace()
        print(data)

        if type(data['cpf']) != str or type(data['name']) != str or type(data['vaccine_name']) != str or type(data['health_unit_name']) != str:
            return {"erro": "Dados numéricos são inválidos"}, HTTPStatus.BAD_REQUEST


        if len(data['cpf']) < 11 or len(data['cpf']) > 11:
            return {"erro": "O campo CPF deve possuir apenas 11 digitos"}, HTTPStatus.BAD_REQUEST
        

        list_users = Vaninado.query.all()
        list_cpf = list()
        for client in list_users:
            client.__dict__
            list_cpf.append(client.cpf)

        if data['cpf'] in list_cpf:
            return {"error": f"Pessoa do cpf {data['cpf']}  já esta cdastrada"}, HTTPStatus.CONFLICT

        vaccinator = Vaninado(**data)

        current_app.db.session.add(vaccinator)
        current_app.db.session.commit()

        return jsonify(vaccinator), HTTPStatus.CREATED
    except KeyError:
        return {"erro": f"Os campos {Vaninado.keys} são obrigatórios para realizar o cadastro"}, HTTPStatus.BAD_REQUEST
    

def get_all():
    person_list = Vaninado.query.all()
    return jsonify(person_list), HTTPStatus.OK