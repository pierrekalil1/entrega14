"""Create table vaccine_cards

Revision ID: 02526923b064
Revises: 
Create Date: 2021-11-26 13:26:24.587966

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02526923b064'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('vaccine_cards',
    sa.Column('cpf', sa.String(length=11), nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('first_shot_date', sa.DateTime(), nullable=True),
    sa.Column('second_shot_date', sa.DateTime(), nullable=True),
    sa.Column('vaccine_name', sa.String(length=255), nullable=False),
    sa.Column('health_unit_name', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('cpf')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('vaccine_cards')
    # ### end Alembic commands ###
